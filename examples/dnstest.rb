# frozen_string_literal: true

require 'resolv'
p Resolv.getname '8.8.8.8'
