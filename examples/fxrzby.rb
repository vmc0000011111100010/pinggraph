# frozen_string_literal: true

require 'fox16'
include Fox
require 'pp'

NUMBERS = (1..9).to_a
ALPHABET_LOWER = ('a'..'z').to_a
ALPHABET_UPPER = ('A'..'Z').to_a
ALL_POSSIBLE_CHARS = (33..126).map(&:chr)

class ClockWidget < FXVerticalFrame
  def initialize(parentFrame)
    super(parentFrame,
      FRAME_SUNKEN | LAYOUT_FILL_X | LAYOUT_FILL_Y | LAYOUT_TOP | LAYOUT_LEFT,
      padLeft: 10, padRight: 10, padTop: 10, padBottom: 10)

    setTime

    @canvas = FXCanvas.new(self, opts: LAYOUT_FILL_X | LAYOUT_FILL_Y | LAYOUT_TOP | LAYOUT_LEFT)

    @canvas.connect(SEL_PAINT) do |_sender, _sel, event|
      FXDCWindow.new(@canvas, event) do |dc|
        radius = [event.rect.w, event.rect.h].min / 4
        centerX = event.rect.w / 2
        centerY = event.rect.h / 2
        hourX = centerX + 0.3 * radius * Math.cos(toRadian(@hourHand))
        hourY = centerY + 0.3 * radius * Math.sin(toRadian(@hourHand))
        minuteX = centerX + 0.6 * radius * Math.cos(toRadian(@minuteHand))
        minuteY = centerY + 0.6 * radius * Math.sin(toRadian(@minuteHand))
        secondX = centerX + 0.75 * radius * Math.cos(toRadian(@secondHand))
        secondY = centerY + 0.75 * radius * Math.sin(toRadian(@secondHand))
        dc.foreground = FXRGB(255, 255, 255)
        dc.fillRectangle(event.rect.x, event.rect.y, event.rect.w, event.rect.h)
        dc.foreground = FXRGB(0, 0, 0)
        dc.fillCircle(centerX, centerY, radius)
        dc.foreground = FXRGB(0, 255, 0)
        dc.drawLine(centerX, centerY, hourX, hourY)
        dc.drawLine(centerX, centerY, minuteX, minuteY)
        dc.foreground = FXRGB(255, 0, 0)
        dc.drawLine(centerX, centerY, secondX, secondY)
      end
    end
  end

  def update
    super
  end

  def setTime
    @hourHand = DateTime.now.hour * 5
    @minuteHand = DateTime.now.min
    @secondHand = DateTime.now.sec
  end

  def tick
    @secondHand = (@secondHand + 1) % 60
    @minuteHand = (@minuteHand + 1) % 60 if @secondHand == 0
    @hourHand = (@hourHand + 5) % 60 if @minuteHand == 0
    @canvas.update
  end

  def toRadian(x)
    (x * Math::PI / 30.0) - (Math::PI / 2.0)
  end
end

class PasswordGenerator < FXMainWindow
  require 'net/ping'
  attr_reader :duration
  def pinging(host)
    @icmp = Net::Ping::ICMP.new(host)
  end

  def duration
    @icmp.ping
    @icmp.duration.to_s
  end

  def mean
    rtary = []
    pingfails = 0
    repeat = 5
    (1..@repeat).each do
      if @icmp.ping
        rtary << icmp.duration

      else
        @pingfails += 1
        puts 'timeout'
      end
      avg = rtary.inject(0) { |sum, i| sum + i } / (repeat - pingfails)
    end
    avg
  end

  def initialize(app)
    super(app, 'Password generator', width: 1000, height: 800)

    pinging('8.8.8.8')

    # hFrame1 = FXHorizontalFrame.new(self)
    #  chrLabel = FXLabel.new(hFrame1, "Number of characters in password:")
    #   chrTextField = FXTextField.new(hFrame1, 4)

    pingframe = FXHorizontalFrame.new(self, width: 600, height: 100, opts: LAYOUT_FILL_X)

    pingmeanArea = FXText.new(pingframe, opts: TEXT_READONLY | TEXT_WORDWRAP | LAYOUT_FILL, width: 600)
    # fuckoff = FXText.new(vFrame2, :opts => LAYOUT_FILL | TEXT_READONLY | TEXT_WORDWRAP)
    # fuckoff.appendText("fuckoff")

    #  hFrame2 = FXHorizontalFrame.new(self)
    #  specialChrsCheck = FXCheckButton.new(hFrame2, "Include special characters in password")

    vFrame1 = FXVerticalFrame.new(self)
    @canvas = FXCanvas.new(vFrame1, opts: LAYOUT_FILL_X | LAYOUT_FILL_Y | LAYOUT_TOP | LAYOUT_LEFT)
    @draw = FXDCWindow.new(@canvas)
    pp(@draw.methods)
    # textArea = FXText.new(vFrame1, :opts =>  TEXT_READONLY | TEXT_WORDWRAP| LAYOUT_FILL)
    #  pingtextArea = FXText.new(vFrame1, :opts => LAYOUT_FILL | TEXT_READONLY | TEXT_WORDWRAP)

    # hFrame3 = FXHorizontalFrame.new(vFrame1)
    # generateButton = FXButton.new(hFrame3, "Generate")
    # copyButton = FXButton.new(hFrame3, "Copy to clipboard")

    pingframe.backColor = FXRGB(0, 0, 255)
    #  hFrame2.backColor=FXRGB(0,255,0)
    # vFrame1.backColor=FXRGB(0,0,255)

    app.addTimeout(100, repeat: true) do
      pingmeanArea.setText(duration)
    end
    # generateButton.connect(SEL_COMMAND) do
    #   textArea.removeText(0, textArea.length)
    #  pingtextArea.removeText(0, textArea.length)
    #   pingtextArea.appendText(duration)
    puts "host replied in #{@icmp.duration}"
    textArea.appendText(generatePassword(chrTextField.text.to_i, ALL_POSSIBLE_CHARS))

    #  end
  end

  def update
    super
    @pingmeanArea.removeText(0, textArea.length)
    @pingmeanArea.appendText(duration)
  end

  def generatePassword(pwLength, charArray)
    len = charArray.length
    (1..pwLength).map do
      charArray[rand(len)]
    end.join
  end

  def create
    super
    show(PLACEMENT_SCREEN)
  end
  end

if __FILE__ == $PROGRAM_NAME
  FXApp.new do |app|
    PasswordGenerator.new(app)
    app.create
    app.run
  end
end
