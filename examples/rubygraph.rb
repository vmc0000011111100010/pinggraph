# frozen_string_literal: true

require 'rubygems'

require 'fox16'

require 'gruff'

include Fox

application = FXApp.new('View', '')

main = FXMainWindow.new(application, 'View', opts: DECOR_ALL, width: 800, height: 600)

graph = Gruff::Line.new

graph.title = 'My Graph'

graph.data('Apples', [1, 2, 3, 4, 4, 3])

graph.data('Oranges', [4, 8, 7, 9, 8, 9])

graph.data('Watermelon', [2, 3, 1, 5, 6, 8])

graph.data('Peaches', [9, 9, 10, 8, 7, 9])

graph.labels = { 0 => '2003', 2 => '2004', 4 => '2005' }

blob = graph.to_blob

graphImage = FXPNGImage.new(application, blob)

graphImage.create

imagesCanvas = FXCanvas.new(main, nil, 0, LAYOUT_FILL_X | LAYOUT_FILL_Y)

imagesCanvas.connect(SEL_PAINT) do |canvas, _sel, ev|
  dc = FXDCWindow.new(canvas, ev)

  dc.drawImage(graphImage, 0, 0)
end

application.create

main.show(PLACEMENT_SCREEN)

application.run
