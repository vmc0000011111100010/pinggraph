# frozen_string_literal: true

# plottter

require 'fox16'
# Simple graph widget
class McCanvas < Fox::FXCanvas
  def initialize(parent, width, height)
    super(parent, opts: FRAME_LINE | LAYOUT_FIX_WIDTH | LAYOUT_FIX_HEIGHT)
    self.height = height
    self.width = width
  end

  # following not used at the moment
  def draw(drc, canvas)
    drc.lineWidth = 5
    drc.setForeground(FXRGB(255, 255, 255))
    drc.fillRectangle(0, 0, canvas.width, canvas.height)
    drc.setForeground(FXRGB(0, 0, 0))
    # sanity check
    drc.drawLines(lines)
  end
end
