# frozen_string_literal: true

# A collection of classes and functions to load and save application data
module McDataloader
  CONFIGFILE = 'config/config.txt'
  IP_REG = /\b(?:\d{1,3}\.){3}\d{1,3}\b/.freeze
  require 'fileutils'
  def self.directorymaker(path)
    # checks for a directory and creates it if not exiting
    dirname = File.dirname(path)
    return if File.directory?(dirname)

    puts "Cant find config #{path}"
    puts 'Atempting to create default directory'
    FileUtils.mkdir_p(dirname)
  end

  # This class loads and saves a config file containing the ip addresse's
  # of Host for the application to ping.
  class Hostloader
    require 'set'
    attr_reader :hosts

    def initialize
      @hosts = []
      McDataloader.directorymaker(CONFIGFILE)

      if File.exist?(CONFIGFILE)
        puts 'Reading config and loading hosts'
      else
        puts 'no config.txt found'
        puts 'creating default config'
        File.open(CONFIGFILE, 'w') do |file|
          file.puts 'Files to load:{'
          file.puts '8.8.8.8}'
        end
      end
      sleep(0.01) # the system locks up with filecreation sometimes
      @hosts = loadhosts(CONFIGFILE)
      puts 'ready'
    end

    def loadhosts(filename)
      hosts = Array.new(0)
      File.foreach(filename) do |line|
        candidate = search_line_for_hosts(line)
        hosts << candidate if candidate
      end
      hosts
    end

    def search_line_for_hosts(line)
      line.scan(IP_REG)[0]
    end

    def savehosts(hosts)
      buffer = []
      pendingsave = hosts
      puts "saving #{pendingsave}"
      array = loadhosts(CONFIGFILE)
      pendingsave.each do |host|
        buffer.push(host) unless array.include?(host)
      end
      buffer = array + buffer
      File.open(CONFIGFILE, 'w') do |file|
        file.truncate(0)
        file.puts 'Files to load:{'
        buffer.each { |item| file.puts item.to_s }
        file.puts '}'
      end
    end

    def savehostsnew(hosts)
      s1 = hosts.to_set
      s2 = loadhosts(CONFIGFILE).to_set
      p "saving #{s2 - s1}"
      s1.merge(s2)
      File.open(CONFIGFILE, 'w') do |file|
        file.truncate(0)
        file.puts 'Files to load:{'
        s1.each { |item| file.puts item.to_s }
        file.puts '}'
      end
    end

    def readline(filename)
      somearray = Array.new(0)
      File.foreach(filename) { |line| somearray.push line }
      somearray
    end
  end
end
# start by checking for existing config files and directories
# Will create default files and directories if missing
# Now loads config and hosts in array with getter method
# has a savehost function to check an array with the config and adds missing
