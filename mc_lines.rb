# frozen_string_literal: true

require 'fox16'

# a graph line
# for the plotter
class MLine
  attr_accessor :lines
  def initialize(length)
    i = 0
    @lines = []
    while i < length
      @lines << Fox::FXPoint.new(i, 20)
      i += 1
    end
  end

  def draw(fdc, cwidth, cheight, linecolor, backround)
    fdc.lineWidth = 5
    fdc.setForeground(backround)
    fdc.fillRectangle(0, 0, cwidth, cheight)
    fdc.setForeground(linecolor)
    fdc.drawLines(lines)
  end

  def popshift(cwidth, ypos)
    @lines.shift
    @lines.each do |point|
      point.x = point.x - 1
    end
    newp = Fox::FXPoint.new(cwidth, ypos.to_i)
    @lines << newp
  end
end
