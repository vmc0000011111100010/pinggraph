# frozen_string_literal: true

# /env/ruby

require 'fox16'

require_relative 'mc_lines'
require_relative 'mc_ping'
require_relative 'mc_canvas'
require 'resolv'

# A custom layoutmanger based on a Matrix style layout
class Mmatrix < Fox::FXMatrix
  def initialize(par, opts)
    super(par, opts)
    @drawhash = {}
    @samenamecounter = Hash.new(0)
  end

  def new_pingunit(host, cwidth, cheight)
    begin
      resolver = Resolv.getname host.to_s
    rescue StandardError
      resolver = 'noResolve'
    end

    name = resolver
    @samenamecounter[name.to_s] += 1
    name += @samenamecounter[name.to_s].to_s

    @drawhash[name] = []
    @drawhash[name][0] = MLine.new(cwidth)
    @drawhash[name][5] = FXHorizontalFrame.new(self, opts: FRAME_RAISED |
                                                           LAYOUT_FILL_Y)
    @drawhash[name][1] = McCanvas.new(@drawhash[name][5], cwidth, cheight)
    @drawhash[name][2] = Mcping.new(host, 60)
    @drawhash[name][3] = cwidth
    @drawhash[name][4] = cheight

    @drawhash[name][6] = FXGroupBox.new(@drawhash[name][5], name.to_s,
                                        width: 125,
                                        opts: GROUPBOX_TITLE_CENTER |
                                              FRAME_RAISED |
                                              LAYOUT_FILL_Y |
                                              LAYOUT_FIX_WIDTH)
    FXLabel.new(@drawhash[name][6], 'average: ',
                width: 60, opts: LAYOUT_FIX_WIDTH)
    @drawhash[name][7] = FXLabel.new(@drawhash[name][6], '',
                                     opts: LAYOUT_FILL_X)
    FXLabel.new(@drawhash[name][6], 'current: ',
                width: 60,
                opts: LAYOUT_FIX_WIDTH)
    @drawhash[name][8] = FXLabel.new(@drawhash[name][6], ' ',
                                     opts: LAYOUT_FILL_X)

    @drawhash[name][11] = FXRGB(rand(100..255), rand(100..255), rand(100..255))
    @drawhash[name][12] = FXRGB(rand(100..255), rand(100..255), rand(100..255))
  end

  def drawall
    @drawhash.each do |_name, pingunit|
      FXDCWindow.new(pingunit[1]) do |dc|
        pingunit[0].draw(dc, pingunit[3], pingunit[4],
                         pingunit[11], pingunit[12])
      end
    end
  end

  def refreshall
    @drawhash.each do |_name, pingunit|
      pingunit[0].popshift(pingunit[3], pingunit[2].duration)
      temp = pingunit[2].pingaverage.round(3)
      p "the ping is #{temp}"
      pingunit[7].text = "#{temp} ms"
      pingunit[8].text = "#{pingunit[2].pingaverage.round(3)} ms"
    end
  end
end
