# frozen_string_literal: true

require 'net/ping'
# Abstraction class for the 'net' utilies that focuses on ICMP ping
class Mcping
  attr_reader :icmp, :host, :repeat, :packetloss
  def initialize(host, buffersize)
    @host = host
    @packetloss = 0
    @icmp = Net::Ping::External.new(@host)
    @meanarray = []
    (1..buffersize).each do
      @meanarray << 0
    end
  end

  # returns the value of 1 ping in msec or 0 if it fails
  def duration
    if @icmp.ping
      @icmp.duration.round(3) * 1000
    else
      @packetloss += 1
      0
    end
  end

  def pingaverage
    @meanarray.shift
    @meanarray << duration
    @meanarray.sum.fdiv(@meanarray.size)
  end
end
