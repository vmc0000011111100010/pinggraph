# frozen_string_literal: true

class Object
  def metaclass_example
    class << self
      p self
    end
  end
end
Object.metaclass_example
