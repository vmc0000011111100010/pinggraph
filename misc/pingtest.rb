# frozen_string_literal: true

require 'Open3'
require 'json'

def remove_non_ascii(string, replacement = '-')
  if string.nil?
    replacement
  else
    a = string.encode('UTF-16be', invalid: :replace, replace: replacement).encode('UTF-8')
  end
end

def blocking
  counter = 0
  pingarray = []
  host = '8.8.8.8'
  Open3.popen2('ping', host.to_s, '/t') do |stdin, stdout, _wait_thr|
    # while !wait_thr.value.exited?
    # File.open("popPing.json", "w") { |file|

    # puts stdout.readline
    # }
    # regex for ping : /(^|\b)Antwort von 8\.8\.8\.8: Bytes=32 Zeit=17ms TTL=119(^|\b)/
    # /\b\d{1,4}ms\b/

    # p wait_thr.value.exited?
    while (stdout.eof? == false) && (counter < 10)
      case remove_non_ascii(stdout.readline)
      when nil?
        pingarray << 0
      when /\b\d{1,4}ms\b/
        a = $LAST_MATCH_INFO
        a = a.to_s
        a.slice!('ms')
        puts "there are #{a} monkey"
        end
      p counter
      counter += 1
    end
    stdin.close # stdin, stdout and stderr should be closed explicitly in this form.
    stdout.close
    p 'closed'
  end
end
blocking
