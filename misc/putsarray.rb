# frozen_string_literal: true

arrray = %i[pid to_s kill exit \[\] \[\]= keys abort_on_exception= abort_on_exception report_on_exception= pending_interrupt? terminate run raise wakeup fetch priority= thread_variable_set thread_variables thread_variable? join alive? key? safe_level priority name= thread_variable_get stop? report_on_exception group backtrace backtrace_locations inspect add_trace_func status name value set_trace_func to_json instance_variable_set instance_variable_defined? remove_instance_variable instance_of? kind_of? is_a? tap instance_variable_get singleton_method method public_method define_singleton_method public_send extend pp to_enum enum_for <=> === =~ !~ eql? respond_to? freeze object_id send display nil? hash class singleton_class clone dup itself yield_self taint tainted? untaint untrust untrusted? trust frozen? methods singleton_methods protected_methods private_methods public_methods instance_variables ! equal? instance_eval == instance_exec != __id__ __send__]
arrray.each do |e|
  puts e
end
