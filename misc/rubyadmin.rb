# frozen_string_literal: true

require 'win32ole'
class Getadmin
  def initialize(filename)
    @filename = filename.to_s
    shell = WIN32OLE.new('Shell.Application')
    shell.ShellExecute('ruby', @filename, nil, 'runas')
  end
end
