#!/usr/bin/ruby
# frozen_string_literal: true

# Simple clock made in FXRuby
require 'pp' # remove in final version
require 'fox16'
include Fox

require_relative 'mc_matrix'
require_relative 'mc_dataloader'

class MainWindow < FXMainWindow
  ENTER_CODE = 65_293

  def initialize(app, preload)
    # config loader and saver
    @preload = preload
    super(app, 'Ping graph', width: 550, height: 450)
    # save before closing
    connect(SEL_CLOSE, method(:on_close))
    @groupbox = FXGroupBox.new self, 'tools',
                               opts:
                               GROUPBOX_NORMAL | FRAME_RAISED | LAYOUT_FILL_X
    @scroll = FXScrollWindow.new(self, opts: LAYOUT_FILL)
    @matrix = Mmatrix.new @scroll,
                          n: 4,
                          opts: LAYOUT_FILL_COLUMN | MATRIX_BY_COLUMNS
    @list = FXListBox.new(@groupbox,
                          opts: LISTBOX_NORMAL | FRAME_SUNKEN | FRAME_THICK)
    @button1 = FXButton.new @groupbox, 'hey man',
                            width: 50,
                            height: 20,
                            opts: FRAME_RAISED | FRAME_THICK | LAYOUT_CENTER_X |
                                  LAYOUT_CENTER_Y

    ip_target = FXDataTarget.new('some text 2')
    FXLabel.new(@groupbox, 'New ip to ping')
    @text_host = FXTextField.new(@groupbox, 10, ip_target, FXDataTarget::ID_VALUE)
    FXLabel.new(@groupbox, 'lookup ip of website')
    @test_lookup = FXTextField.new(@groupbox, 11)

    @button1.connect(SEL_COMMAND) do |_sender, _sel, _data|
      validate_ip(@text_host, ip_target.value)
    end

    ip_target.connect(SEL_COMMAND) do |_sender, _sel, data|
      validate_ip(@text_host, data)
    end

    @list.connect(SEL_COMMAND) do |sender, _sel, data|
      +
      ip_target.value = sender.getItemText(data)
    end

    preload.hosts.each { |host| @list.appendItem(host) }
    app.addTimeout(900, repeat: true) do
      @matrix.refreshall
    end
    app.addTimeout(90, repeat: true) do
      @matrix.drawall
    end
  end

  def validate_ip(field, host)
    p host
    if host =~ McDataloader::IP_REG
      addhost(host)
    else
      wrong_input(field)
    end
  end

  def wrong_input(tfield)
    tfield.backColor = FXRGB(252, 57, 3)
    app.addTimeout(450, repeat: false) do
      tfield.backColor = Fox.FXRGB(255, 255, 255)
    end
  end

  def addhost(host)
    p 'creating new new_pingunit'
    @matrix.new_pingunit(host, 400, 400)
    @matrix.create
    add_item_to(host, @list)
  end

  def add_item_to(item, list)
    if list.findItem(item) == -1
      list.prependItem(item)
    else
      @text_host.baseColor = FXRGB(205, 92, 92)
      app.addChore do
        @groupbox.baseColor = FXRGB(0, 0, 0)
      end
    end
  end

  def on_close(_sender, _sel, _ptr)
    puts 'shuting down'
    buffer = []
    @list.each { |text, _icon, _data| buffer.push(text) }
    # p @preload.savehostsnew(buffer)[0]
    @preload.savehostsnew(buffer)
    getApp.exit(0)
  end

  def create
    super
    show(PLACEMENT_SCREEN)
    @matrix.drawall
  end
end

if __FILE__ == $PROGRAM_NAME
  preload = McDataloader::Hostloader.new
  FXApp.new do |app|
    window = MainWindow.new(app, preload)
    app.addSignal('SIGINT', window.method(:save))
    app.create
    app.run
  end
end
